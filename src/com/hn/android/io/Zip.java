package com.hn.android.io;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Zip
{
	public static boolean unpackZip(String zipfile)
	{
		return unpackZip(new File(zipfile));
	}

	public static boolean unpackZip(File zipfile)
	{
		return unpackZip(zipfile, zipfile.getParentFile());
	}

	public static boolean unpackZip(String zipfile, String outpath)
	{
		return unpackZip(new File(zipfile), new File(outpath));
	}

	public static boolean unpackZip(File zipfile, File outpath)
	{
		try
		{
			return unpackZip(new FileInputStream(zipfile), outpath);
		}
		catch(FileNotFoundException e)
		{
			return false;
		}
	}

	public static boolean unpackZip(InputStream stream, File outpath)
	{
		ZipInputStream zipstream;

		try
		{
			zipstream = new ZipInputStream(new BufferedInputStream(stream));

			byte[] buffer = new byte[1024];
			String filename = null;
			int count = 0;

			ZipEntry ze = null;

			while((ze = zipstream.getNextEntry()) != null)
			{
				FileOutputStream fout = null;

				try
				{
					filename = ze.getName();

					File outFile =
						new File(outpath, filename);

					if(ze.isDirectory())
					{
						outFile.mkdir();
						continue;
					}

					fout =
						new FileOutputStream(outFile);

					while((count = zipstream.read(buffer)) != -1)
						fout.write(buffer, 0, count);
				}
				finally
				{
					if(fout != null)
					{
						fout.flush();
						fout.close();
					}

					zipstream.closeEntry();
				}
			}

			zipstream.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();

			return false;
		}

		return true;
	}
}
