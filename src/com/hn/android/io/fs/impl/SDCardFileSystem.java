package com.hn.android.io.fs.impl;

import java.io.File;

import android.content.Context;
import android.os.Environment;

public class SDCardFileSystem extends DefaultFileSystem
{
	public SDCardFileSystem(Context con)
	{
		super(con);
	}

	@Override
	public boolean isVolumeReadable()
	{
		String EXT_STATE =
			Environment.getExternalStorageState();

		return Environment.MEDIA_MOUNTED.equals(EXT_STATE)
			|| Environment.MEDIA_MOUNTED_READ_ONLY.equals(EXT_STATE);
	}

	@Override
	public boolean isVolumeWritable()
	{
		String EXT_STATE =
			Environment.getExternalStorageState();

		return Environment.MEDIA_MOUNTED.equals(EXT_STATE);
	}

	@Override
	public File rootDir()
	{
		return getDir(null);
	}

	@Override
	public File tempDir()
	{
		return _context.getExternalCacheDir();
	}

	@Override
	public File picsDir()
	{
		return getDir(Environment
			.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), getAppName());
	}

	@Override
	public File getDir(String name)
	{
		return _context.getExternalFilesDir(name);
	}
}
