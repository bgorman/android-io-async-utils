package com.hn.android.io.fs;

import java.io.File;
import java.io.InputStream;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

import com.hn.android.io.fs.impl.DefaultFileSystem;
import com.hn.android.io.fs.impl.DeviceFileSystem;
import com.hn.android.io.fs.impl.SDCardFileSystem;

/*
 * Class to manage device and external (SD card) filesystems
 * as well as app's persistent shared preferences. Once
 * initialised with a Context, app can access its filesystems
 * from anywhere using e.g. FS.sd().tempDir()
 */
public class FS
{
	public static final String TAG = "android-io-async-utils FS: ";

	public static final int DEFAULT_MODE = Context.MODE_PRIVATE;

	public static final String	EXT_ZIP = ".zip", EXT_XML = ".xml",
								EXT_JPG = ".jpg", EXT_PNG = ".png";

	public static final String	MIME_IMG = "image/*";

	// locations of download and persistent data directories
	// individual filesystem objects create PICS and TEMP dirs
	public static final String	DIR_DL = Environment.DIRECTORY_DOWNLOADS,
								DIR_DATA = "data";

	private static DefaultFileSystem dev, sd, wfs, rfs;

	private static Context _context;

/* APP CONTEXT */
	public static void setContext(Context con)
	{
		_context = con;

		dev = new DeviceFileSystem(_context);
		sd = new SDCardFileSystem(_context);

		wfs = (sd.isVolumeWritable() ? sd : dev);
		rfs = (sd.isVolumeReadable() ? sd : dev);
	}

	public static Context context()
	{
		return _context;
	}

/* FILESYSTEMS */

	// FS.dev().readFile("test.jpg")
	public static DefaultFileSystem dev()
	{
		return dev;
	}

	// FS.sd().readFile("test.jpg")
	public static DefaultFileSystem sd()
	{
		return sd;
	}

	// return sd() if writable, otherwise dev()
	public static DefaultFileSystem wfs()
	{
		return wfs;
	}

	// return sd() if readable, otherwise dev()
	public static DefaultFileSystem rfs()
	{
		return rfs;
	}

/* PREFERENCES */
	public static SharedPreferences getPreferences()
	{
		return getPreferences(DEFAULT_MODE);
	}

	public static SharedPreferences getPreferences(int mode)
	{
		return (_context == null ? null :
			_context.getSharedPreferences(_context.getApplicationInfo().name, mode));
	}

	public static SharedPreferences.Editor getPreferencesEditor()
	{
		return (_context == null ? null : getPreferences().edit());
	}

	public static boolean setPref(String key, Object value)
	{
		if(_context == null || value == null)
			return false;

		if(value instanceof Boolean)
			getPreferencesEditor().putBoolean(key, (Boolean)value).commit();
		else if(value instanceof Float)
			getPreferencesEditor().putFloat(key, (Float)value).commit();
		else if(value instanceof Integer)
			getPreferencesEditor().putInt(key, (Integer)value).commit();
		else if(value instanceof Long)
			getPreferencesEditor().putLong(key, (Long)value).commit();
		else if(value instanceof String)
			getPreferencesEditor().putString(key, (String)value).commit();
		else
			return false;

		return true;
	}

	@SuppressWarnings("unchecked")
	public static <E> E getPref(String key, E defaultval)
	{
		if(_context != null)
		{
			if(defaultval instanceof Boolean)
				return (E)(Boolean)getPreferences().getBoolean(key, (Boolean)defaultval);
			else if(defaultval instanceof Float)
				return (E)(Float)getPreferences().getFloat(key, (Float)defaultval);
			else if(defaultval instanceof Integer)
				return (E)(Integer)getPreferences().getInt(key, (Integer)defaultval);
			else if(defaultval instanceof Long)
				return (E)(Long)getPreferences().getLong(key, (Long)defaultval);
			else if(defaultval instanceof String)
				return (E)getPreferences().getString(key, (String)defaultval);
		}

		return defaultval;
	}

/* SHARED PUBLIC FS */
	public static File getSharedPublicDir(String type)
	{
		return Environment.getExternalStoragePublicDirectory(type);
	}

/* RAW FILES */
	public static InputStream readRawFile(int resourceID)
	{
		return (_context == null ? null :
			_context.getResources().openRawResource(resourceID));
	}

/* MISC */
	public static void cleanup()
	{
		if(_context != null)
		{
			dev.cleanup();
			sd.cleanup();
		}
	}
}
